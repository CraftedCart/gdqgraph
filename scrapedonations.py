#!/usr/bin/env python3

import urllib.request
import re
import datetime
import json

url = "https://gamesdonequick.com/tracker/donations/agdq2019?page={index}"
strip_tags_re = re.compile("<.*?> *")

donation_data = []


def main():
    global donation_data

    for i in range(930):
        print(f"Fetching page {i + 1} / 930")

        prev_donation_data = donation_data.copy()
        try:
            fetch_page(i + 1)
        except ValueError:
            print("Here we go again")
            donation_data = prev_donation_data
            fetch_page(i + 1)

    with open("donations.json", "w") as f:
        f.write(json.dumps(donation_data))


def fetch_page(page: int):
    global donation_data

    # Need a regular browser user agent else we get a 403
    req = urllib.request.Request(
        url.format(index=page),
        data=None,
        headers={
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) ' +
            'AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47 ' +
            'Safari/537.36'
        }
    )

    response = urllib.request.urlopen(req)
    data = response.read().decode('utf-8')

    lines = data.splitlines()
    active_entry = {}

    # Loop over each line to find the values
    is_searching = False
    i = 0
    data_i = 0
    for line in lines:
        if line == "</tr>":  # Look for the end of the table header row
            is_searching = True
        elif line == ("<td>") and is_searching:
            value = re.sub(strip_tags_re, "", lines[i + 1])

            if data_i == 0:
                active_entry["name"] = value
            elif data_i == 1:
                active_entry["time"] = int(datetime.datetime.strptime(value, "%m/%d/%Y %H:%M:%S +0000").timestamp())
            elif data_i == 2:
                active_entry["amount"] = int(value[1:].replace(".", "").replace(",", ""))
            elif data_i == 3:
                active_entry["has_comment"] = value == "Yes"

            data_i += 1
            if data_i == 4:
                data_i = 0
                donation_data.append(active_entry)
                active_entry = {}

        i += 1


if __name__ == "__main__":
    main()
