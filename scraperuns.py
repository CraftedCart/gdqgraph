#!/usr/bin/env python3

import urllib.request
import re
import datetime
import json


url = "https://gamesdonequick.com/tracker/runs/agdq2019"
run_data = []
strip_tags_re = re.compile("<.*?> *")


def main():
    global run_data

    # Need a regular browser user agent else we get a 403
    req = urllib.request.Request(
        url,
        data=None,
        headers={
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) ' +
            'AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47 ' +
            'Safari/537.36'
        }
    )

    response = urllib.request.urlopen(req)
    data = response.read().decode('utf-8')

    lines = data.splitlines()
    active_entry = {}

    # Loop over each line to find the values
    is_searching = False
    i = 0
    data_i = 0
    for line in lines:
        if line == "</tr>":  # Look for the end of the table header row
            is_searching = True
        elif line == ("<td>") and is_searching:
            value = re.sub(strip_tags_re, "", lines[i + 1])

            if data_i == 0:
                active_entry["name"] = value
            elif data_i == 1:
                active_entry["players"] = value
            elif data_i == 3:
                active_entry["start"] = int(datetime.datetime.strptime(value, "%m/%d/%Y %H:%M:%S +0000").timestamp())
            elif data_i == 4:
                active_entry["end"] = int(datetime.datetime.strptime(value, "%m/%d/%Y %H:%M:%S +0000").timestamp())

            data_i += 1
            if data_i == 6:
                data_i = 0
                run_data.append(active_entry)
                active_entry = {}

        i += 1

    with open("runs.json", "w") as f:
        f.write(json.dumps(run_data))


if __name__ == "__main__":
    main()
