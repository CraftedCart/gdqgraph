gdqgraph
========

## AGDQ 2019 donations

![AGDQ 2019 graph](graph.png)

## How do?

- Set up your venv
- Install requirements
- Run scrapedonations.py
- Hope nothing explodes
- Run plot.py

I tried plotting each run (hence the scraperuns.py file, but it didn't look too good - have a look at allthelabels.png)
