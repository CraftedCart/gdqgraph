#!/usr/bin/env python3

import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import json
from datetime import datetime
# import colorsys


def main():
    with open("donations.json", "r") as f:
        data = json.loads(f.read())

    # with open("runs.json", "r") as f:
    #     runs = json.loads(f.read())

    x = []
    y = []

    total = 0
    for entry in data[::-1]:
        x.append(datetime.utcfromtimestamp(entry["time"]))
        total += entry["amount"] / 100
        y.append(total)

    fig, ax = plt.subplots()

    # ax.plot(x, y)
    plt.xlabel("Time")
    plt.ylabel("Amount raised")
    plt.title("AGDQ 2019 donations over time")
    plt.xticks(rotation=90)
    plt.yticks([i for i in range(0, 2500001, 50000)])
    plt.grid(alpha=0.2)

    rule = mdates.rrulewrapper(mdates.HOURLY, interval=2)
    loc = mdates.RRuleLocator(rule)
    ax.xaxis.set_major_locator(loc)

    ax.xaxis.set_major_formatter(mdates.DateFormatter("%a %d | %H:%M"))

    # h = 0
    # s = 0.8
    # v = 0.8
    # for run in runs:
    #     ax.fill_between(
    #         [datetime.utcfromtimestamp(run["start"]), datetime.utcfromtimestamp(run["end"])],
    #         0,
    #         2500000,
    #         color=colorsys.hsv_to_rgb(h, s, v),
    #         alpha=0.5
    #         )

    #     h += 0.22
    #     if h > 1:
    #         h -= 1

    ax.fill_between(x, 0, y, color="#504bb8")

    plt.show()


if __name__ == "__main__":
    main()
